# Используем Node.js как базовый образ
FROM node:14

# Устанавливаем рабочую директорию
WORKDIR /app

# Копируем файлы проекта
COPY package*.json ./
RUN npm install

COPY . .

# Запускаем бота
CMD ["node", "bot.js"]