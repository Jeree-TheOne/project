const { Bot } = require('grammy');

const bot = new Bot('YOUR_TELEGRAM_BOT_TOKEN'); // Замените на ваш токен

// Ответит "Привет!" на любое сообщение.
bot.on('message', (ctx) => ctx.reply('Привет!'));

bot.start();